<?php
declare(strict_types=1);

namespace Lith\Contacts\ViewModel;

use Magento\Contact\Helper\Data;
use Magento\Contact\ViewModel\UserDataProvider;

class UserDataProviderExtended extends UserDataProvider
{

    private Data $helper;

    /**
     * UserDataProviderExtended constructor.
     *
     * @param Data $helper
     */
    public function __construct(Data $helper)
    {
        parent::__construct($helper);
        $this->helper = $helper;
    }

    /**
     * Get user company name
     *
     * @return string
     */
    public function getUserCompany(): string
    {
        return $this->helper->getPostValue('company');
    }
}
