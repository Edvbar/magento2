<?php

namespace Lith\Products\Plugin\Catalog\Model;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\Config\ScopeConfigInterface;

class ProductPlugin
{

    private ScopeConfigInterface $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function afterGetName(Product $subject, $result): string
    {
        $title = '';

        if ($textByProduct = $subject->getCustomAttribute('text_by_products')->getValue()) {
            $title .= $textByProduct . ' ';
        }

        $title .= $result;

        if ($textAfterProduct = $this->scopeConfig->getValue('lithproducts/general/display_text')) {
            $title .= " " . $textAfterProduct;
        }

        return $title;
    }
}
